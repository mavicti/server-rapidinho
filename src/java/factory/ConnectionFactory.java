package factory;

import java.sql.Connection;
import java.sql.DriverManager;
 
public class ConnectionFactory {
 
   private static final String USERNAME = "adminsCIVIBm"; 
   private static final String PASSWORD = "bPj2YG4PvWjv";
 
   private static final String DATABASE_URL = "jdbc:mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/rapidinho";
   
   public static Connection createConnectionToMySQL() throws Exception{
      Class.forName("com.mysql.jdbc.Driver");  
      Connection connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
 
      return connection;
   }
   public static void main(String[] args) throws Exception{
 
      Connection con = createConnectionToMySQL(); 
      if(con != null){
         System.out.println("Conexão obtida com sucesso!" + con);
         con.close();
      }
 
   }
}