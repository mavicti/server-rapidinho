package util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;


public final class Utilidades {

    /**
     * Método responsável por pegar um texto e deixar somente a primeira letra
     * do texto sendo maiúscula
     *
     * @param texto
     * @return Retorna o texto passado pelo usuário, deixando somente a primeira letra maiúscula
     * @author Vinicius Lino Nunes dos Santos
     * @version 1.0
     * @since 26/04/2015
     */
    public static String primeiraMaiuscula(String texto) {
        char[] textoArray = null; // declaração de um array de char[] pra separar as letras da String
        textoArray = texto.toCharArray(); // o método toCharArray() transforma a String passada em um array já declarado
        String textoModificado1 = null; // String que vai pegar a primeira letra do texto e deixar maiúscula
        String textoModificado2 = null; // String que vai pegar o resto do texto

        for (int i = 0; i < texto.length(); i++) {
            if (i != 0) { // condição dentro do 'for' pra quando não entrar quando for a primeira letra
                // copia o valor do char dentro do array 'textoArray', com inicio no índice 1, do tamanho do texto original -1 caracter	(que vai ser maiúsculo)
                textoModificado2 = String.copyValueOf(textoArray, 1, texto.length() - 1);
            } else { // condição dentro do 'for' pra entrar quando for a primeira letra
                // copia o valor do char dentro do array 'textoArray', com inicio no índice 0 e tamanho 1
                textoModificado1 = String.copyValueOf(textoArray, 0, 1);
            }
        }

        // concatena os dois textos, deixando a primeira letra maiúscula e o resto minúscula
        texto = textoModificado1.toUpperCase() + textoModificado2.toLowerCase();
        return texto;
    }

    /**
     * Método responsável por remover todos os "\n" (código que faz pular linha) do texto. Também diminui a quantidade de espaços, caso tenham multiplos espaços no texto
     *
     * @param texto
     * @return Retorna o texto passado sem pular linhas
     * @author Vinicius Lino Nunes dos Santos
     * @version 1.0
     * @since 02/05/2015
     */
    public static String semPularLinha(String texto) {
        texto = texto.replaceAll("\n", " "); // pega o texto e troca todos os "\n" (pula linha) por " " (espaço simples)
        for (int i = 0; i < 3; i++) { // inicia um laço de repetição de 3 vezes
            texto = texto.replaceAll("  ", " "); // pega o texto e troca todos os "  " (espaços duplos) por " " (espaço simples)
        }
        return texto;
    }

    /**
     * Método responsável por validar o idioma inglês no aplicativo
     *
     * @param texto
     * @return Retorna "true" caso o idioma seja identificado como sendo Inglês, ou "false" caso seja outro idioma
     * @author Vinicius Lino Nunes dos Santos
     * @version 1.0;
     * @since 15/05/2015
     */
    public static boolean idiomaIngles(String texto) {
        int res = texto.compareTo("English");
        if (res == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método responsável por validar se os caracteres digitados são números
     *
     * @param valor
     * @return Retorna "true" caso sejam apenas números, ou "false" caso não sejam
     * @author Vinicius Lino Nunes dos Santos
     * @version 1.0;
     * @since 05/11/2015
     */
    private static boolean validaSoNumeros(Double valor) {
        boolean validacaoNumeros = true;
        String valorString = valor.toString();
        char[] valorArray = valorString.toCharArray();

        for (int i = 0; i < valorString.length(); i++) {
            if (valorArray[i] != '0' || valorArray[i] != '1' || valorArray[i] != '2' || valorArray[i] != '3' || valorArray[i] != '4' || valorArray[i] != '5' || valorArray[i] != '6' || valorArray[i] != '7' || valorArray[i] != '8' || valorArray[i] != '9' || valorArray[i] != '.') {
                validacaoNumeros = false;
            }
        }

        return validacaoNumeros;
    }

    public static Date parseDate(String data, String mascara) throws ParseException {
        if (data != null && !"".equals(data)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat();

            if (mascara != null && !"".equals(mascara) && data.length() > 10) {
                dateFormat.applyPattern(mascara);
            } else {
                dateFormat.applyPattern("dd/MM/yyyy");
            }
            dateFormat.setLenient(false);
            return dateFormat.parse(data);
        }
        return null;
    }

    public static String parseDate(Date data, String mascara) throws ParseException {
        if (data != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat();

            if (mascara != null && !"".equals(mascara)) {
                dateFormat.applyPattern(mascara);
            } else {
                dateFormat.applyPattern("dd/MM/yyyy");
            }
            dateFormat.setLenient(false);
            return dateFormat.format(data); // <-- O format é o que difere do outro método.
        }
        return null;
    }

    public static boolean isNuloOuVazio(Object valor) {
        if (valor instanceof String) {
            String string = (String) valor;
            if (string == null || string.replace("/", "").replace(".", "").replace("-", "").replace(":", "").trim().isEmpty()) {
                return true;
            }
        } else if (valor instanceof ArrayList) {
            ArrayList arrayList = (ArrayList) valor;
            if (arrayList == null || arrayList.isEmpty()) {
                return true;
            }
        } else if (valor instanceof HashSet) {
            HashSet setList = (HashSet) valor;
            if (setList == null || setList.isEmpty()) {
                return true;
            }
        } else {
            return valor == null;
        }

        return false;
    }

    public static boolean isCpfValido(String CPF) {
        if (!isNuloOuVazio(CPF)) {
            CPF = CPF.replace(".", "").replace("-", "").trim();
        }

        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") || CPF.equals("22222222222") || CPF.equals("33333333333") || CPF.equals("44444444444") || CPF.equals("55555555555") || CPF.equals("66666666666") || CPF.equals("77777777777") || CPF.equals("88888888888") || CPF.equals("99999999999") || (CPF.length() != 11)) {
            return (false);
        }
        char dig10, dig11;
        int sm, i, r, num, peso;
        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            } else {
                dig10 = (char) (r + 48);
            }
            // converte no respectivo caractere numerico
            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            } else {
                dig11 = (char) (r + 48);
            }
            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }
    }    
}
