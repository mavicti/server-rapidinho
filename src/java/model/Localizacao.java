package model;

public class Localizacao {
    
    private Integer _id;
    private String latitude;
    private String longitude;
    private String ultimaDataLocalizacao;

    public Integer getId() {
        return _id;
    }

    public void setId(Integer _id) {
        this._id = _id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUltimaDataLocalizacao() {
        return ultimaDataLocalizacao;
    }

    public void setUltimaDataLocalizacao(String ultimaDataLocalizacao) {
        this.ultimaDataLocalizacao = ultimaDataLocalizacao;
    }
}
