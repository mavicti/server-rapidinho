package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Produto;

public class ProdutoDAO {

    public void save(Produto produto) {
        String sql = "INSERT INTO produtos (nome_produto, preco, descricao) VALUES(?,?,?)";

        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();

            pstm = conn.prepareStatement(sql);

            pstm.setString(1, produto.getNomeProduto());
            pstm.setDouble(2, produto.getPreco());
            pstm.setString(3, produto.getDescricao());

            pstm.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void removeById(int id) {
        String sql = "DELETE FROM produtos WHERE _id = ?";

        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();
            pstm = conn.prepareStatement(sql);
            pstm.setInt(1, id);
            pstm.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void update(Produto produto) {
        String sql = "UPDATE produtos SET nome_produto = ?, preco = ?, descricao = ? WHERE _id = ?";

        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();

            pstm = conn.prepareStatement(sql);

            pstm.setString(1, produto.getNomeProduto());
            pstm.setDouble(2, produto.getPreco());
            pstm.setString(3, produto.getDescricao());
            pstm.setInt(4, produto.get_id());

            pstm.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<Produto> getListaProdutos() {

        String sql = "SELECT * FROM produtos";

        List<Produto> produtos = new ArrayList<Produto>();

        Connection conn = null;
        PreparedStatement pstm = null;

        ResultSet rset = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();
            pstm = conn.prepareStatement(sql);
            rset = pstm.executeQuery();

            while (rset.next()) {
                Produto produto = new Produto();

                produto.set_id(rset.getInt("_id"));
                produto.setNomeProduto(rset.getString("nome_produto"));
                produto.setPreco(rset.getDouble("preco"));
                produto.setDescricao(rset.getString("descricao"));

                produtos.add(produto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {
                    rset.close();
                }

                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return produtos;
    }
    
    public Produto searchById(Integer id){
        String sql = "SELECT * FROM produtos WHERE _id = ?";
        Connection conn = null;
        PreparedStatement pstm = null;
        Produto produto = new Produto();
        ResultSet rset = null;
        
        try{
            
            conn = ConnectionFactory.createConnectionToMySQL();    
            pstm = conn.prepareStatement(sql);
            
            pstm.setInt(1, id);
            
            rset = pstm.executeQuery();
            
             while (rset.first()) {                
                produto.set_id(rset.getInt("_id"));
                produto.setNomeProduto(rset.getString("nome_produto"));
                produto.setPreco(rset.getDouble("preco"));
                produto.setDescricao(rset.getString("descricao"));
            }
            
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {
                    rset.close();
                }

                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        
        return produto;
    }
    
}
