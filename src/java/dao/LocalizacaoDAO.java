package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Localizacao;

public class LocalizacaoDAO {

    public void save(Localizacao localizacao) {
        String sql = "INSERT INTO localizacao (_id, latitude, longitude, ultimaDataLocalizacao) VALUES(?,?,?,?)";

        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();

            pstm = conn.prepareStatement(sql);

            pstm.setInt   (0, localizacao.getId());
            pstm.setString(1, localizacao.getLatitude());
            pstm.setString(2, localizacao.getLongitude());
            pstm.setString(3, localizacao.getUltimaDataLocalizacao());

            pstm.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void removeById(int id) {
        String sql = "DELETE FROM localizacao WHERE _id = ?";

        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();
            pstm = conn.prepareStatement(sql);
            
            pstm.setInt(0, id);
            
            pstm.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void update(Localizacao localizacao, Integer id) {

        String sql = "UPDATE localizacao SET latitude = ?, longitude = ?, ultimaDataLocalizacao = ? WHERE _id = ?";

        Connection conn = null;
        PreparedStatement pstm = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();

            pstm = conn.prepareStatement(sql);

            pstm.setString(1, localizacao.getLatitude());
            pstm.setString(2, localizacao.getLongitude());
            pstm.setString(3, localizacao.getUltimaDataLocalizacao());
            pstm.setInt(4, 1);

            pstm.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<Localizacao> getListaLocalizacoes() {
        String sql = "SELECT * FROM localizacao";

        List<Localizacao> localizacoes = new ArrayList<Localizacao>();

        Connection conn = null;
        PreparedStatement pstm = null;

        ResultSet rset = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();
            pstm = conn.prepareStatement(sql);
            rset = pstm.executeQuery();

            if (rset.first()) {
                Localizacao localizacao = new Localizacao();

                localizacao.setId(rset.getInt("_id"));
                localizacao.setLatitude(rset.getString("latitude"));
                localizacao.setLongitude(rset.getString("longitude"));
                localizacao.setUltimaDataLocalizacao(rset.getString("ultimaDataLocalizacao"));

                localizacoes.add(localizacao);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {
                    rset.close();
                }

                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return localizacoes;
    }
    
    public Localizacao searchById(Integer id){
        String sql = "SELECT * FROM localizacao WHERE _id = ?";

        Localizacao localizacao = new Localizacao();
        
        Connection conn = null;
        PreparedStatement pstm = null;

        ResultSet rset = null;

        try {
            conn = ConnectionFactory.createConnectionToMySQL();
            pstm = conn.prepareStatement(sql);
            pstm.setInt(1, id);
            
            rset = pstm.executeQuery();

            while (rset.first()) {                
                localizacao.setId(rset.getInt("_id"));
                localizacao.setLatitude(rset.getString("latitude"));
                localizacao.setLongitude(rset.getString("longitude"));
                localizacao.setUltimaDataLocalizacao(rset.getString("ultimaDataLocalizacao"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {
                    rset.close();
                }

                if (pstm != null) {
                    pstm.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return localizacao;
    }
            
}
