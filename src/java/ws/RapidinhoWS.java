package ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import dao.LocalizacaoDAO;
import dao.ProdutoDAO;
import dao.StatusDAO;
import dao.UsuarioDAO;
import java.util.Date;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import model.Localizacao;
import model.Produto;
import model.Status;
import model.Usuario;
import util.Utilidades;

@Path("rapidinho")
public class RapidinhoWS {

    @Context
    private UriInfo context;

    public RapidinhoWS() {
    }
    /*
    POST = Inserir 
    PUT = Alterar
    DELETE = Excluir
    GSON = Cleber
    */
    @GET
    @Produces("application/json")
    @Path("/localizacao/inserir/{latitude}/{longitude}")
    public String setLocalizacao(@PathParam("latitude") String latitude, @PathParam("longitude") String longitude) {
        Localizacao localizacao = new Localizacao();
        try {
            LocalizacaoDAO dao = new LocalizacaoDAO();
            localizacao.setLatitude(latitude);
            localizacao.setLongitude(longitude);
            localizacao.setUltimaDataLocalizacao(Utilidades.parseDate(new Date(), "dd/MM/yyyy - HH:mm"));

            if (dao.getListaLocalizacoes().isEmpty()) {
                dao.save(localizacao);
            } else {
                dao.update(localizacao, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson g = new Gson();
        return g.toJson("Sucesso");
    }

    // O consumidor passará o id que deseja buscar, ou seja (0)
    @GET
    @Produces("application/json")
    @Path("/localizacao")
    public String getLocalizacao() {
        LocalizacaoDAO dao = new LocalizacaoDAO();
        List<Localizacao> local = null;
        Gson g = new Gson();

        try {
            local = dao.getListaLocalizacoes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return g.toJson(local);
    }

    @GET
    @Produces("application/json")
    @Path("/status/inserir")
    public String setStatus() {
        Status status = new Status();
        try {
            StatusDAO dao = new StatusDAO();

            if (!dao.getListaStatus().isEmpty()) {
                if (dao.getListaStatus().get(0).getStatus() == 1) {
                    status.setStatus(0);
                    dao.update(status, 1);
                } else {
                    status.setStatus(1);
                    dao.update(status, 1);
                }
            } else {
                status.set_id(1);
                status.setStatus(0);
                dao.save(status);
            }

            if (dao.getListaStatus().isEmpty()) {
                dao.save(status);
            } else {
                dao.update(status, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson g = new Gson();
        return g.toJson("Sucesso");
    }

    @GET
    @Produces("application/json")
    @Path("/status")
    public String getStatus() {
        StatusDAO dao = new StatusDAO();
        List<Status> status = null;
        Gson g = new Gson();

        try {
            status = dao.getListaStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return g.toJson(status);
    }

    @GET
    @Produces("application/json")
    @Path("/produtos/inserir/{nome}/{preco}/{descricao}")
    public String insertProduto(@PathParam("nome") String nome, @PathParam("preco") Double preco,
            @PathParam("descricao") String descricao) {
        Produto produto = new Produto();
        ProdutoDAO dao = new ProdutoDAO();

        try {
            produto.setNomeProduto(nome);
            produto.setPreco(preco);
            produto.setDescricao(descricao);

            dao.save(produto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson g = new Gson();
        return g.toJson("Sucesso");
    }

    @GET
    @Produces("application/json")
    @Path("/produtos/alterar/{id}/{nome}/{preco}/{descricao}")
    public String updateProduto(@PathParam("id") Integer id, @PathParam("nome") String nome,
            @PathParam("preco") Double preco, @PathParam("descricao") String descricao) {
        ProdutoDAO dao = new ProdutoDAO();
        Produto produto = new Produto();
        Gson g = new Gson();

        try {
            produto.set_id(id);
            produto.setNomeProduto(nome);
            produto.setPreco(preco);
            produto.setDescricao(descricao);

            dao.update(produto);
        } catch (Exception e) {
            e.printStackTrace();
            return g.toJson("Falha");
        }

        return g.toJson("Sucesso");
    }

    @GET
    @Produces("application/json")
    @Path("/produtos/excluir/{id}")
    public String removeProduto(@PathParam("id") Integer id) {
        Gson g = new Gson();

        try {
            ProdutoDAO dao = new ProdutoDAO();
            dao.removeById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return g.toJson("Falha");
        }

        return g.toJson("Sucesso");
    }

    @GET
    @Produces("application/json")
    @Path("/produtos")
    public String listProdutos() {
        List<Produto> produtos = new ArrayList<>();
        ProdutoDAO dao = new ProdutoDAO();
        Gson g = new Gson();

        try {
            produtos = dao.getListaProdutos();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return g.toJson(produtos);
    }

    @GET
    @Produces("application/json")
    @Path("/usuario/alterar/{id}/{nome}/{login}/{senha}")
    public String updateUsuario(@PathParam("id") Integer id, @PathParam("nome") String nome,
            @PathParam("login") String login, @PathParam("senha") String senha) {
        UsuarioDAO dao = new UsuarioDAO();
        Usuario usuario = new Usuario();
        Gson g = new Gson();

        try {
            usuario.set_id(id);
            usuario.setNome(nome);
            usuario.setLogin(login);
            usuario.setSenha(senha);

            dao.update(usuario);
        } catch (Exception e) {
            e.printStackTrace();
            return g.toJson("Falha");
        }

        return g.toJson("Sucesso");
    }

    @GET
    @Produces("application/json")
    @Path("/usuario/inserir/{nome}/{login}/{senha}")
    public String saveUsuario(@PathParam("nome") String nome,
            @PathParam("login") String login, @PathParam("senha") String senha) {
        UsuarioDAO dao = new UsuarioDAO();
        Usuario usuario = new Usuario();
        Gson g = new Gson();

        try {
            usuario.setNome(nome);
            usuario.setLogin(login);
            usuario.setSenha(senha);

            dao.save(usuario);

        } catch (Exception e) {
            e.printStackTrace();
            return g.toJson("Falha");
        }
        return g.toJson("Sucesso");
    }

    @GET
    @Produces("application/json")
    @Path("usuario/delete/{id}")
    public String deleteUsuario(@PathParam("id") Integer id) {
        UsuarioDAO dao = new UsuarioDAO();
        Gson g = new Gson();

        try {
            dao.removeById(id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return g.toJson("Sucesso");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        throw new UnsupportedOperationException();
    }
}
